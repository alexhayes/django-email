# Django Email

A reusable Django app that provides abstract models that provide email sending and tracking.

Currently in alpha state.

## Installation

	pip install git+git://bitbucket.org/alexhayes/django-email

Then add `django_generic_token` to your installed apps.

## Migration

Django Generic Token uses South for managing database migration, so:

	./manage.py migrate django_email

## Author

Alex Hayes <alex@alution.com>  