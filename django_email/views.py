from django.views.generic.detail import DetailView
from django.core.exceptions import ImproperlyConfigured
from sendfile import sendfile
from django_generic_token.models import Token
from django_email.models import DetectedOpen
from django.contrib.staticfiles import finders
from django.templatetags.static import static
import os
import logging
logger = logging.getLogger(__name__)

class DetectedOpenView(DetailView):
    model = Token
    slug_field = 'token'
    slug_url_kwarg = 'token'
    filepath = None
    
    def get_filepath(self):
        if self.filepath is None:
            raise ImproperlyConfigured("Either get_filepath() or attribute 'filepath' must be set.")
        return self.filepath
    
    def create_detected_open(self, request, *args, **kwargs):
        tokened_email = self.object.tokened_email
        if not tokened_email.is_verified():
            tokened_email.verify()
        DetectedOpen.objects.create(email=tokened_email)
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.create_detected_open(request, *args, **kwargs)
        return sendfile(request, self.get_filepath())