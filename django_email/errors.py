class FileNotFoundError(Exception): pass
class AlreadySentError(Exception): pass
class SendError(Exception): pass