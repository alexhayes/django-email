from django.db import models
from django.contrib.auth import get_user_model
from django.conf import settings
import logging
from templated_email import get_templated_mail
from django.utils import timezone
from django_generic_token.models import Token
from django.db.models.query import QuerySet
from django_email.errors import AlreadySentError, SendError
from model_utils.managers import PassThroughManager

logger = logging.getLogger(__name__)

class EmailQuerySet(QuerySet):
    def queued(self):
        return self.filter(status=Email.STATUS_QUEUED)
    def considered_not_sent(self):
        return self.filter(status__in=Email.STATUS_CONSIDERED_NOT_SENT)
    def sent(self):
        return self.filter(status=Email.STATUS_SENT)
    def failed(self):
        return self.filter(status=Email.STATUS_SENT)

class Email(models.Model):
    """
    Represents a single email.
    """
    STATUS_QUEUED = 'queued'   # Queued for deliver
    STATUS_SENDING = 'sending' # Currently sending
    STATUS_SENT = 'sent'       # Sent
    STATUS_FAILED = 'failed'   # Failed sending
    STATUS_CHOICES = (
        (STATUS_QUEUED, 'Queued'),
        (STATUS_SENDING, 'Sending'),
        (STATUS_SENT, 'Sent'),
        (STATUS_FAILED, 'Failed'),
    )
    STATUS_CONSIDERED_NOT_SENT = (STATUS_QUEUED, STATUS_FAILED)
    STATUS_CONSIDERED = (STATUS_SENT,)
    STATUS_CONSIDERED_UPDATABLE = (STATUS_FAILED,)

    to = models.EmailField(max_length=255, null=True, blank=True)
    status = models.CharField(max_length=128, choices=STATUS_CHOICES, default=STATUS_QUEUED)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=True)
    sent = models.DateTimeField(null=True, blank=True)

    objects = PassThroughManager.for_queryset_class(EmailQuerySet)()

    def __unicode__(self):
        return '%s (%s): %s' % (self.to, self.pk, self.get_status_display())
    
    def is_sent(self):
        return self.status == self.STATUS_SENT

    def get_email_template(self):
        """
        Overload this method and return a tuple, the first value being the template prefix the second the template name.
        """
        raise NotImplementedError('get_email_template() must be defined for %s.' % self.__class__) 

    def get_email_recipients(self):
        """
        Overload this method and return a list of recipients ie.. ('John Doe <john@example.com>', ...) 
        """
        if settings.DEBUG:
            # Send email out to admins only.
            return ['%s <%s>' % a for a in settings.ADMINS]
        else:
            return [self.to]

    def get_email_to(self):
        """
        Overload this method and return the email address of the recipient.
        """
        raise NotImplementedError('get_email_to() must be defined for %s.' % self.__class__) 

    def get_email_from(self):
        """
        Overload this method and return a from email address.
        """
        raise NotImplementedError('get_email_from() must be defined for %s.' % self.__class__) 

    def get_email_context(self, to, from_email, **kwargs):
        """
        Overload this method and return a dict of context vars to be applied to the email template
        """
        return dict(
            to=to,
            from_email=from_email,
            **kwargs
        )

    def get_templated_email_kwargs(self):
        template_prefix, template_name = self.get_email_template()
        to = self.get_email_recipients()
        from_email = self.get_email_from()
        context = self.get_email_context(to=to,
                                         from_email=from_email)
        
        return dict(
            template_name=template_name,
            template_prefix=template_prefix,
            context=context,
            from_email=from_email,
            to=to
        )

    def email_sending(self):
        """
        Called before attempting to send the email.
        
        Primary role is to 'lock' this model so that no one else attempts to send
        it.
        """
        self.status = self.STATUS_SENDING
        self.save(update_fields=('status', 'updated'))        

    def email_send_success(self, **kwargs):
        """
        Called when an email is successfully sent.
        """
        self.status = self.STATUS_SENT
        self.sent = timezone.now()
        self.save(update_fields=('status', 'updated', 'sent'))

    def email_send_failure(self, exc_info, **kwargs):
        """
        Called when an email fails to send.
        """
        self.status = self.STATUS_FAILED
        self.save(update_fields=('status', 'updated'))

    def send(self, force=False):
        """
        Send the email.
        """
        if not force and self.status in self.STATUS_CONSIDERED:
            raise AlreadySentError("%s '%s' already sent." % (self._meta.verbose_name, self.pk))

        templated_email_kwargs = {}

        try:
            self.email_sending()

            templated_email_kwargs = self.get_templated_email_kwargs()

            msg = get_templated_mail(**templated_email_kwargs)
            
            if msg.send():
                self.email_send_success(**templated_email_kwargs)
            else:
                raise SendError("Couldn't send email '%s' to '%s'." % (self.pk, templated_email_kwargs.get('to')))

        except Exception as e:
            self.email_send_failure(e, **templated_email_kwargs)
            raise

class TokenedEmail(Email):
    """
    A Email that includes a link to redeem a token - using django-generic-token
    """
    token = models.OneToOneField(Token, related_name='tokened_email')
    verified = models.DateTimeField(help_text="The date this tokened email was initially verified.", blank=True, null=True)

    def is_verified(self):
        return self.verified is not None
    
    def verify(self):
        self.verified = timezone.now()
        self.save(update_fields=('verified',))

class Open(models.Model):
    """Represents the opening of an email"""
    email = models.ForeignKey(Email)
    opened = models.DateTimeField(auto_now_add=True)

class DetectedOpen(Open):
    """
    Represents the detected opening of an email.
    
    This differs from the Open model in that this model is created when it's 
    detected that the recipient opened an email, ie.. via a image within the email
    verses Open which could be created by the creation of a Click. 
    """

class Click(models.Model):
    """Represents the clicking of a link in an email."""
    email = models.ForeignKey(Email, related_name='clicks')
    url = models.URLField(max_length=255)
    clicked = models.DateTimeField(auto_now_add=True)