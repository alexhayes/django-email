# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Email.status'
        db.alter_column(u'django_email_email', 'status', self.gf('django.db.models.fields.CharField')(max_length=128))

        # Changing field 'Email.to'
        db.alter_column(u'django_email_email', 'to', self.gf('django.db.models.fields.EmailField')(max_length=255, null=True))

    def backwards(self, orm):

        # Changing field 'Email.status'
        db.alter_column(u'django_email_email', 'status', self.gf('django.db.models.fields.CharField')(max_length=16))

        # Changing field 'Email.to'
        db.alter_column(u'django_email_email', 'to', self.gf('django.db.models.fields.EmailField')(default='', max_length=255))

    models = {
        u'django_email.click': {
            'Meta': {'object_name': 'Click'},
            'clicked': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'clicks'", 'to': u"orm['django_email.Email']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '255'})
        },
        u'django_email.detectedopen': {
            'Meta': {'object_name': 'DetectedOpen', '_ormbases': [u'django_email.Open']},
            u'open_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['django_email.Open']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'django_email.email': {
            'Meta': {'object_name': 'Email'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sent': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'queued'", 'max_length': '128'}),
            'to': ('django.db.models.fields.EmailField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'})
        },
        u'django_email.open': {
            'Meta': {'object_name': 'Open'},
            'email': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['django_email.Email']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'opened': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'django_email.tokenedemail': {
            'Meta': {'object_name': 'TokenedEmail', '_ormbases': [u'django_email.Email']},
            u'email_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['django_email.Email']", 'unique': 'True', 'primary_key': 'True'}),
            'token': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'tokened_email'", 'unique': 'True', 'to': u"orm['django_generic_token.Token']"}),
            'verified': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'django_generic_token.token': {
            'Meta': {'ordering': "('-created',)", 'object_name': 'Token'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sent': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'token_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'tokens'", 'to': u"orm['django_generic_token.TokenType']"})
        },
        u'django_generic_token.tokentype': {
            'Meta': {'object_name': 'TokenType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_redemptions': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'token_length': ('django.db.models.fields.IntegerField', [], {'default': '16'})
        }
    }

    complete_apps = ['django_email']