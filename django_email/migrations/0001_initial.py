# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django_generic_token', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Click',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField(max_length=255)),
                ('clicked', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Email',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('to', models.EmailField(max_length=255, null=True, blank=True)),
                ('status', models.CharField(default=b'queued', max_length=128, choices=[(b'queued', b'Queued'), (b'sending', b'Sending'), (b'sent', b'Sent'), (b'failed', b'Failed')])),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True, auto_now_add=True)),
                ('sent', models.DateTimeField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Open',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('opened', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DetectedOpen',
            fields=[
                ('open_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='django_email.Open')),
            ],
            options={
            },
            bases=('django_email.open',),
        ),
        migrations.CreateModel(
            name='TokenedEmail',
            fields=[
                ('email_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='django_email.Email')),
                ('verified', models.DateTimeField(help_text=b'The date this tokened email was initially verified.', null=True, blank=True)),
                ('token', models.OneToOneField(related_name='tokened_email', to='django_generic_token.Token')),
            ],
            options={
            },
            bases=('django_email.email',),
        ),
        migrations.AddField(
            model_name='open',
            name='email',
            field=models.ForeignKey(to='django_email.Email'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='click',
            name='email',
            field=models.ForeignKey(related_name='clicks', to='django_email.Email'),
            preserve_default=True,
        ),
    ]
