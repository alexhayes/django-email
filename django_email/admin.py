from django.contrib import admin
from django_email.models import Email, TokenedEmail, Open, DetectedOpen, Click

class EmailAdmin(admin.ModelAdmin):
    list_display = ('to', 'status', 'created', 'updated', 'sent')
admin.site.register(Email, EmailAdmin)

class TokenedEmailAdmin(admin.ModelAdmin):
    list_display = ('to', 'token', 'status', 'created', 'updated', 'sent')
admin.site.register(TokenedEmail, TokenedEmailAdmin)

class OpenAdmin(admin.ModelAdmin):
    list_display = ('email', 'opened')
admin.site.register(Open, OpenAdmin)

class DetectedOpenAdmin(admin.ModelAdmin):
    list_display = ('email', 'opened')
admin.site.register(DetectedOpen, DetectedOpenAdmin)

class ClickAdmin(admin.ModelAdmin):
    list_display = ('email', 'url', 'clicked')
admin.site.register(Click, ClickAdmin)